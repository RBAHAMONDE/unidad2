/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.unidad2eval2.dao.MasterClientesJpaController;
import com.mycompany.unidad2eval2.entity.MasterCliente;
import com.mycompany.unidad2eval2.entity.MasterCliente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Simon navarro
 */
@WebServlet(name = "MasterControlador", urlPatterns = {"/MasterControlador"})
public class MasterControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MasterControlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MasterControlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("do Post");
       
            
                String identificacion = request.getParameter("identificacion");
                String nombres= request.getParameter("nombres");
                String apellido1 = request.getParameter("apellido1");
                String apellido2 = request.getParameter("apellido2");
                String rut = request.getParameter("rut");
                
                
                MasterCliente cliente = new MasterCliente();
                cliente.setCliIdentificacion(identificacion);
                cliente.setCliNombres(nombres);
                cliente.setCliApellido1(apellido1);
                cliente.setCliApellido2(apellido2);
                cliente.setCliRut(rut);
                
                MasterClientesJpaController dao=new MasterClientesJpaController();
                try {
                dao.create(cliente);      
                
                Logger.getLogger("log").log(Level.INFO,"valor identificacion cliente:(0)",cliente.getCliIdentificacion());
                 } catch (Exception ex){
                 Logger.getLogger("log").log(Level.SEVERE,"Se presenta un error al intentar ingresar cliente:(0)",ex.getMessage());    
                 }
       
        
        request.getRequestDispatcher("salida.jsp").forward(request, response); 
            }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
