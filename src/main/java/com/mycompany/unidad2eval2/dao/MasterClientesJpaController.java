/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unidad2eval2.dao;

import com.mycompany.unidad2eval2.exceptions.NonexistentEntityException;
import com.mycompany.unidad2eval2.exceptions.PreexistingEntityException;
import com.mycompany.unidad2eval2.entity.MasterCliente;
import com.mycompany.unidad2eval2.entity.MasterCliente;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Simon navarro
 */
public class MasterClientesJpaController implements Serializable {

    public MasterClientesJpaController(EntityManagerFactory emf){
      this.emf = emf;
         
    }

    public MasterClientesJpaController() {
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
  
      public EntityManager getEntityManager() {
        return emf.createEntityManager();
            
     }
 

    public void create(MasterCliente masterClientes) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(masterClientes);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMasterClientes(masterClientes.getCliIdentificacion()) != null) {
                throw new PreexistingEntityException("MasterClientes " + masterClientes + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MasterCliente masterClientes) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            masterClientes = em.merge(masterClientes);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = masterClientes.getCliIdentificacion();
                if (findMasterClientes(id) == null) {
                    throw new NonexistentEntityException("The masterClientes with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MasterCliente masterClientes;
            try {
                masterClientes = em.getReference(MasterCliente.class, id);
                masterClientes.getCliIdentificacion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The masterClientes with id " + id + " no longer exists.", enfe);
            }
            em.remove(masterClientes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MasterCliente> findMasterClientesEntities() {
        return findMasterClientesEntities(true, -1, -1);
    }

    public List<MasterCliente> findMasterClientesEntities(int maxResults, int firstResult) {
        return findMasterClientesEntities(false, maxResults, firstResult);
    }

    private List<MasterCliente> findMasterClientesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MasterCliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MasterCliente findMasterClientes(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MasterCliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getMasterClientesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MasterCliente> rt = cq.from(MasterCliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

   
    
}
