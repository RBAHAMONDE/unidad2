/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.unidad2eval2.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Simon navarro
 */
@Entity
@Table(name = "master_clientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MasterCliente.findAll", query = "SELECT m FROM MasterCliente m"),
    @NamedQuery(name = "MasterCliente.findByCliIdentificacion", query = "SELECT m FROM MasterCliente m WHERE m.cliIdentificacion = :cliIdentificacion"),
    @NamedQuery(name = "MasterCliente.findByCliNombres", query = "SELECT m FROM MasterCliente m WHERE m.cliNombres = :cliNombres"),
    @NamedQuery(name = "MasterCliente.findByCliApellido1", query = "SELECT m FROM MasterCliente m WHERE m.cliApellido1 = :cliApellido1"),
    @NamedQuery(name = "MasterCliente.findByCliApellido2", query = "SELECT m FROM MasterCliente m WHERE m.cliApellido2 = :cliApellido2"),
    @NamedQuery(name = "MasterCliente.findByCliRut", query = "SELECT m FROM MasterCliente m WHERE m.cliRut = :cliRut")})
public class MasterCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "cli_identificacion")
    private String cliIdentificacion;
    @Size(max = 2147483647)
    @Column(name = "cli_nombres")
    private String cliNombres;
    @Size(max = 2147483647)
    @Column(name = "cli_apellido1")
    private String cliApellido1;
    @Size(max = 2147483647)
    @Column(name = "cli_apellido2")
    private String cliApellido2;
    @Column(name = "cli_rut")
    private String cliRut;

    public MasterCliente() {
    }

    public MasterCliente(String cliIdentificacion) {
        this.cliIdentificacion = cliIdentificacion;
    }

    public String getCliIdentificacion() {
        return cliIdentificacion;
    }

    public void setCliIdentificacion(String cliIdentificacion) {
        this.cliIdentificacion = cliIdentificacion;
    }

    public String getCliNombres() {
        return cliNombres;
    }

    public void setCliNombres(String cliNombres) {
        this.cliNombres = cliNombres;
    }

    public String getCliApellido1() {
        return cliApellido1;
    }

    public void setCliApellido1(String cliApellido1) {
        this.cliApellido1 = cliApellido1;
    }

    public String getCliApellido2() {
        return cliApellido2;
    }

    public void setCliApellido2(String cliApellido2) {
        this.cliApellido2 = cliApellido2;
    }

    public String getCliRut() {
        return cliRut;
    }

    public void setCliRut(String cliRut) {
        this.cliRut = cliRut;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliIdentificacion != null ? cliIdentificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MasterCliente)) {
            return false;
        }
        MasterCliente other = (MasterCliente) object;
        if ((this.cliIdentificacion == null && other.cliIdentificacion != null) || (this.cliIdentificacion != null && !this.cliIdentificacion.equals(other.cliIdentificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.unidad2eval2.entity.MasterCliente[ cliIdentificacion=" + cliIdentificacion + " ]";
    }
    
}
